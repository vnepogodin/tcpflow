# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Chris Brannon <cmbrannon79@gmail.com>
# Contributor: Jeff Mickey <jeff@archlinux.org>

pkgname=tcpflow
pkgver=1.6.1
pkgrel=3
pkgdesc="Captures data transmitted as part of TCP connections then stores the data conveniently"
arch=('x86_64')
url="https://github.com/simsong/tcpflow"
license=('GPL')
depends=('glibc' 'gcc-libs' 'openssl' 'sqlite'
         'libpcap' 'libcap-ng' 'zlib' 'cairo')
makedepends=('git' 'boost')
source=("git+https://github.com/simsong/$pkgname.git#tag=$pkgname-$pkgver"
        'git+https://github.com/simsong/be13_api.git'
        'git+https://github.com/simsong/dfxml.git'
        'git+https://github.com/joyent/http-parser.git')
sha512sums=('011735821364544de127f0d44360256db31ab3717c89af657f6a9608de29fa0d335bb8bb2473160dfdd7245f351bd37a17698964dcc426c747f6644c67900e6d'
            'SKIP'
            'SKIP'
            'SKIP')

prepare() {
  cd $pkgname
  git submodule init
  git config submodule."src/be13_api".url "$srcdir/be13_api"
  git config submodule."src/dfxml".url "$srcdir/dfxml"
  git config submodule."src/http-parser".url "$srcdir/http-parser"
  git -c protocol.file.allow=always submodule update

  # fix issue detected by -Werror=format-security
  git format-patch -1 --stdout a0697509c465 | patch -Np1

  # fix build with GCC 13
  git format-patch -1 --stdout b1479db14b16 | patch -Np1

  autoreconf -vi
}

build() {
  cd $pkgname
  sh bootstrap.sh
  ./configure --prefix=/usr --mandir=/usr/share/man
  make
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
}
